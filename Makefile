SHELL := /bin/bash

install:
	python setup.py install

build:
	pip install -e . 

test:
	python -m pytest ./foo

lint:
	flake8 ./foo

package:
	python setup.py bdist_wheel --universal

deploy:
	twine upload \
		--repository-url $(TWINE_REPOSITORY) \
		-u $(TWINE_USERNAME) \
		-p $(TWINE_PASSWORD) \
		dist/*

clean:
	rm -rf dist/ || true
	rm -rf build/ || true
	rm -rf *.egg-info || true
