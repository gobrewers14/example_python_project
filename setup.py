import foo
import setuptools

VERSION = foo.__version__

setuptools.setup(
    name="foo",
    version=VERSION,
    author="foo",
    author_email="bar@foo.com",
    zip_safe=False,
    platforms="any",
    install_requires=[],
)
