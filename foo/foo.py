class Foo:
    """``Foo``."""
    def __init__(self, x):
        self.x = x

    def __call__(self):
        print(f"the variable x := {self.x}")
        return self.x
