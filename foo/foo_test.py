from foo.foo import Foo
from absl.testing import absltest as test_lib


class FooTest(test_lib.TestCase):
    def setUp(self):
        self.foo = Foo(14)

    def test_foo(self):
        self.assertEqual(self.foo(), 14)


if __name__ == "__main__":
    test_lib.main()
